# LFSS - Loi de financement de la Sécurité sociale
<!-- SPDX-License-Identifier: MPL-2.0 -->

La loi de financement de la sécurité sociale (LFSS) est votée chaque année par le Parlement ; elle fixe l’objectif national des dépenses d’assurance maladie (<link-previewer href="ONDAM.html" text="Ondam" preview-title="ONDAM - Objectif national des dépenses d’assurance maladie" preview-text="L’ONDAM est fixé chaque année par le Parlement, conformément aux dispositions de la loi de financement de la sécurité sociale (LFSS. " />).

# Références

- [Page wikipedia](https://fr.wikipedia.org/wiki/Loi_de_financement_de_la_S%C3%A9curit%C3%A9_sociale)
- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/lfss-loi-de-financement-de-la-securite-sociale) sur le site internet du ministère
