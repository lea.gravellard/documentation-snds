# GHT - Groupe homogène de tarifs
<!-- SPDX-License-Identifier: MPL-2.0 -->

Le groupe homogène de tarifs correspond aux tarifs journaliers applicables en hospitalisation à domicile (<link-previewer href="HAD.html" text="HAD" preview-title="HAD - Hospitalisation à domicile" preview-text="L’hospitalisation à domicile (HAD) est une hospitalisation à temps complet au cours de laquelle les soins sont effectués au domicile de la personne. " />). 
Le GHT est un « tout compris » obtenu en affectant à chaque groupe homogène de prise en charge (<link-previewer href="GHPC.html" text="GHPC" preview-title="GHPC - Groupe homogène de prise en charge" preview-text="Combinaison de 3 variables composant un séjour en hospitalisation à domicile : " />), un indice de pondération destiné à tenir compte de la durée de prise en charge.

# Références

- [Fiche](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/financement-des-etablissements-de-sante-10795/financement-des-etablissements-de-sante-glossaire/article/groupe-homogene-de-tarifs-ght) sur le site internet du ministère
